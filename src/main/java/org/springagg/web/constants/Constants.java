package org.springagg.web.constants;

public interface Constants {
    String SESSION_FORCE_LOGOUT_KEY = "session.force.logout";
}
